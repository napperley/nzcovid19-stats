group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("js") version "1.3.72"
}

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    val kotlinVer = "1.3.72"
    val fritz2Ver = "0.4"
    val chartJsVer = "2.8.0"

    implementation(npm(name = "chart.js", version = chartJsVer))
    implementation(kotlin("stdlib-js", kotlinVer))
    implementation("io.fritz2:fritz2-core-js:$fritz2Ver")
}

kotlin {
    target {
        useCommonJs()
        browser {
            @Suppress("EXPERIMENTAL_API_USAGE")
            distribution { directory = file("$projectDir/output") }
        }
    }
}
