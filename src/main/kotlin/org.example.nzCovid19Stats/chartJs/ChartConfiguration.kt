package org.example.nzCovid19Stats.chartJs

external interface ChartConfiguration<D : Number> {
    val type: String
    var data: ChartData<D>
}
