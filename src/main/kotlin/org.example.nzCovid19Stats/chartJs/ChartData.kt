package org.example.nzCovid19Stats.chartJs

external interface ChartData<T : Number> {
    var labels: Array<String>
    var datasets: Array<ChartDataSet<T>>
}
