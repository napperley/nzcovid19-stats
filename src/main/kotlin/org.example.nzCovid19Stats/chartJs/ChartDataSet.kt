package org.example.nzCovid19Stats.chartJs

external interface ChartDataSet<T : Number> {
    val label: String
    val backgroundColor: String
    val borderColor: String
    val data: Array<T>
}
