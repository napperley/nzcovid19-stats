package org.example.nzCovid19Stats.chartJs

import org.w3c.dom.HTMLCanvasElement

external class Chart<D : Number>(context: HTMLCanvasElement, options: ChartConfiguration<D>) {
    fun update(): Unit = definedExternally
}
