package org.example.nzCovid19Stats

import io.fritz2.binding.const
import io.fritz2.dom.html.Button
import io.fritz2.dom.html.Div
import io.fritz2.dom.html.HtmlElements
import io.fritz2.dom.html.render
import io.fritz2.dom.mount

private const val BTN_TYPE = "btn-primary"

private fun HtmlElements.row(
    id: String = "",
    classes: Array<String> = emptyArray(),
    content: Div.() -> Unit
) = div(id = id) {
    content()
    classList =
        if (classes.isNotEmpty()) const(listOf("row", *classes))
        else const(listOf("row"))
}

private fun HtmlElements.bootstrapButton(
    id: String = "",
    btnType: String,
    content: Button.() -> Unit
) = button(id = id) {
    content()
    type = const("button")
    classList = const(listOf("btn", btnType))
}

private fun HtmlElements.mainView() = div(id = "main-layout") {
    className = const("container")
    row(id = "title-row") {
        h1 { text("NZ Covid-19 Statistics") }
    }
    row(id = "btn-layout") { buttonGroup() }
    row(id = "chart-title-row") {
        h2(id = "chart-title") { ChartStore.data.bind() }
    }
    row { canvas(id = "covid19-chart") {} }
}

private fun HtmlElements.buttonGroup() = div {
    className = const("btn-group")
    bootstrapButton("covid19-tests-btn", BTN_TYPE) {
        text("Covid-19 Tests")
        clicks handledBy ChartStore.loadCovid19TestsChart
    }
    bootstrapButton("electronics-imports-btn", BTN_TYPE) {
        text("Electronics Imports")
        clicks handledBy ChartStore.loadElectronicsImportsChart
    }
}

fun main() {
    // Renders the main view, which is attached to the body HTML element.
    render { mainView() }.mount("target")
    loadChart()
}
