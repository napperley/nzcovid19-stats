package org.example.nzCovid19Stats

import org.example.nzCovid19Stats.chartJs.ChartData
import org.example.nzCovid19Stats.chartJs.ChartDataSet

private val chartLabels = arrayOf("March", "April", "May", "June")

internal fun createCovid19TestsChartData() = object : ChartData<Int> {
    override var labels = chartLabels
    override var datasets: Array<ChartDataSet<Int>> = arrayOf(create2019DataSet())
}

private fun create2019DataSet() = object : ChartDataSet<Int> {
    override val label = "Total Tests"
    override val backgroundColor = "rgb(255, 99, 132)"
    override val borderColor = "rgb(255, 99, 132)"
    override val data = arrayOf(209451, 2469318, 6991610, 856527)
}
