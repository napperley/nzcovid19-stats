package org.example.nzCovid19Stats

import org.example.nzCovid19Stats.chartJs.ChartData
import org.example.nzCovid19Stats.chartJs.ChartDataSet

private val chartLabels = (1..29).map { it.toString() }.toTypedArray()
private val electronics2019Data = arrayOf(
    6000000,
    23000000,
    70000000,
    136000000,
    152000000,
    203000000,
    250000000,
    257000000,
    267000000,
    319000000,
    369000000,
    409000000,
    461000000,
    502000000,
    508000000,
    522000000,
    570000000,
    617000000,
    671000000,
    732000000,
    773000000,
    779000000,
    795000000,
    835000000,
    873000000,
    912000000,
    958000000,
    1012000000,
    1018000000
)
private val electronics2020Data = arrayOf(
    18000000,
    26000000,
    98000000,
    159000000,
    229000000,
    241000000,
    294000000,
    303000000,
    317000000,
    359000000,
    414000000,
    461000000,
    526000000,
    569000000,
    575000000,
    590000000,
    627000000,
    678000000,
    725000000,
    760000000,
    817000000,
    823000000,
    842000000,
    886000000,
    931000000,
    962000000,
    1009000000,
    1050000000,
    1057000000
)

internal fun createElectronicsImportsChartData() = object : ChartData<Int> {
    override var labels = chartLabels
    override var datasets: Array<ChartDataSet<Int>> = arrayOf(create2019DataSet(), create2020DataSet())
}

private fun create2019DataSet() = object : ChartDataSet<Int> {
    override val label = "Feb 2019"
    override val backgroundColor = "rgb(255, 99, 132)"
    override val borderColor = "rgb(255, 99, 132)"
    override val data = electronics2019Data
}

private fun create2020DataSet() = object : ChartDataSet<Int> {
    override val label = "Feb 2020"
    override val backgroundColor = "rgb(0,128,0)"
    override val borderColor = "rgb(0,128,0)"
    override val data = electronics2020Data
}
