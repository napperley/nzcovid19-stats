package org.example.nzCovid19Stats

import io.fritz2.binding.RootStore
import org.example.nzCovid19Stats.chartJs.Chart
import org.example.nzCovid19Stats.chartJs.ChartConfiguration
import org.example.nzCovid19Stats.chartJs.ChartData
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document

private val chartConfiguration by lazy {
    object : ChartConfiguration<Int> {
        override val type = "line"
        override var data: ChartData<Int> = createCovid19TestsChartData()
    }
}
private val chart by lazy { Chart(fetchCanvasElement(), chartConfiguration) }

private fun fetchCanvasElement() = document.getElementById("covid19-chart") as HTMLCanvasElement

internal fun loadChart() {
    chart
}

/** Manages chart state. */
internal object ChartStore : RootStore<String>("New Zealand Covid-19 Tests Per Month") {
    val loadCovid19TestsChart = handle {
        chartConfiguration.data = createCovid19TestsChartData()
        chart.update()
        "New Zealand Covid-19 Tests Per Month"
    }

    val loadElectronicsImportsChart = handle {
        chartConfiguration.data = createElectronicsImportsChartData()
        chart.update()
        "New Zealand Electronics Imports Per Day (in NZD)"
    }
}
