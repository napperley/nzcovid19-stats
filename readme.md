# New Zealand Covid19 Statistics (nzcovid-19-stats)

A basic web application that displays some New Zealand Covid-19 statistics in a chart. The application uses Kotlin 
1.3.72 (via Kotlin JS), and the Fritz2 framework.

## Usage

In order to run the program you will need to do the following:

1. Open a terminal
2. Clone this repository
3. Change working directory to the cloned repository directory
4. Run the web application: `./gradlew run`

After performing the steps above you should see the web application running in a new tab on the default web browser.